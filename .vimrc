" ===============================
" =__   _(_)_ __ ___  _ __ ___  =
" =\ \ / / | '_ ` _ \| '__/ __| =
" = \ V /| | | | | | | | | (__  =
" =  \_/ |_|_| |_| |_|_|  \___| =
" ===============================
let mapleader =","
let maplocalleader="\<space>"
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin('~/.vim/plugged')
    Plug 'dracula/vim', { 'as': 'dracula' }
    Plug 'itchyny/lightline.vim'
    Plug 'scrooloose/nerdtree'
    Plug 'myusuf3/numbers.vim'
    Plug 'scrooloose/syntastic'
    Plug 'wikitopian/hardmode'
    Plug 'junegunn/fzf'
    Plug 'vim-scripts/ctags.vim'
    Plug 'brookhong/cscope.vim'
    Plug 'vim-scripts/a.vim'
    Plug 'tpope/vim-surround'
    Plug 'townk/vim-autoclose'
    Plug 'rip-rip/clang_complete'
    Plug 'stanangeloff/php.vim'
    Plug 'shawncplus/phpcomplete.vim'
    Plug 'ap/vim-css-color'
    Plug 'othree/html5.vim'
    Plug 'pangloss/vim-javascript'
    Plug 'mhinz/vim-startify'
    Plug 'junegunn/goyo.vim'
    Plug 'sirver/ultisnips'
    Plug 'honza/vim-snippets'
    Plug 'tpope/vim-fugitive'
call plug#end()
"set configuration
set wrap
set number
set t_Co=256
set autoread
set tabstop=4
set expandtab
set autowrite
set noshowmode
set rtp+=~/.fzf
set laststatus=2
set shiftwidth=4
set encoding=utf-8
set hlsearch
set foldmethod=syntax
set nofoldenable
"noremap configuration
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>
noremap <c-t> :m-2<cr>
noremap <c-d> :m+1<cr>
nnoremap <F4> :NumbersOnOff<CR>
nnoremap <F3> :NumbersToggle<CR>
nnoremap <leader>h <Esc>:call ToggleHardMode()<CR>
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>
"map configuration
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
map <C-n> :NERDTreeToggle<CR>
"etc configuration
syntax on
colo dracula
autocmd BufWritePre * %s/\s\+$//e
let g:lightline = {'colorscheme': 'powerline'}
let g:UltiSnipsExpandTrigger="<tab>"
